""" In this file we have created a command line with the click package. This command line is called hello and takes into argument the list of urls of products and optional argument the name reference and rhe price of the product.
    Authors : Elisa Krammer & Dan Slama """

import click
import os
os.system('python webscraping_project_part2.py')

@click.command()
@click.argument('urls')
@click.option('--name', help='name of the product')
@click.option('--reference', help='reference of the product')
@click.option('--price', help='price of the product')
def cli(urls):
        print('You decided to choose the product urls : %s' % urls)

