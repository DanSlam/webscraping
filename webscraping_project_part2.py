""" This document is using requests and BeautifulSoup to scrape pages from Zabilo website
It has been developed with Pycharm
Authors : Elisa Krammer and Dan Slama """

# Import of packages
import requests
from bs4 import BeautifulSoup
import pandas as pd
import click


def start(url_home_page):
    # This funtion allows to scrape the url of "Today's Hot Deal"
    url_home_page = requests.get(url_home_page)
    content_of_home = url_home_page.content
    soup_of_home = BeautifulSoup(content_of_home, 'html.parser')
    link_of_deals = [i['href'] for i in soup_of_home.select('a[class*="navdeals"]')]
    return link_of_deals


def deal_pages(url_deal):
    # This function allows to scrape the url of the different categories of product
    url_deal = requests.get(url_deal[0])
    content_deal = url_deal.content
    soup_of_deal = BeautifulSoup(content_deal, 'html.parser')
    link_of_category_of_product =[]
    for category in soup_of_deal.select('[class="landing_categories_little"]'):
        for http in category.select('a'):
            link_of_category_of_product.append(http['href'])
    return link_of_category_of_product


def link_of_product(list_of_url):
    # We scrape the link of product pages and outputs a list of product urls
    list_of_product = []
    for url in list_of_url:
        url_product_machine = requests.get(url)
        content_product = url_product_machine.content
        soup_of_product = BeautifulSoup(content_product, 'html.parser')
        for category in soup_of_product.select('a[class*="product-name"]'):
            list_of_product.append(category['href'])
    return list_of_product


def get_product_characteristics(list_url):
    # This function allows to get the characteristics for each url of product and outputs a list of dictionaries.
    # Each of these dictionaries corresponds to the characteristics of one product: price, availability, category etc
    list_product = []
    for url in list_url:

        r = requests.get(url)
        c = r.content
        soup = BeautifulSoup(c, 'html.parser')

        table = soup.find_all("table", {"class": "table-data-sheet data_sheet"})[0]
        reviews = soup.find_all("strong", {"class": "comment_title"})
        reviews = [i.get_text() for i in reviews]
        reviews = reviews[0:len(reviews)//2]

        char_dict = {}
        characteristics = []

        for row in table.find_all('tr'):
            element = row.find_all('td')
            for td in element:
                characteristics.append(td.text)

            keys = []
            values = []
            for i in range(0, len(characteristics)):
                if i % 2 == 0:
                    keys.append(characteristics[i])
                else:
                    values.append(characteristics[i])

            keys = keys[0:14]
            values = values[0:14]

            # add price and reviews
            keys.append('Price')
            values.append(soup.find('span', attrs = {'itemprop':'price'}).get_text())
            keys.append('Old Price')
            values.append(soup.find('span', attrs = {'id':'old_price_display'}).get_text())

            keys.append('Reviews')
            values.append(reviews)

            char_dict = {k: v for k, v in zip(keys, values)}
        list_product.append(char_dict)
    return list_product


def get_data_frame(list_product):
    # We creat a dataframe with every characteristics for each product
    df = pd.DataFrame(list_product)
    df.groupby("Product")
    return df



def main():
    url_home_page = "https://www.zabilo.com/en/"
    # We scrape the link of deal pages
    start(url_home_page)
    # We scrape the url for each category of product in deal pages
    deal_pages(start(url_home_page))
    # We scrape the link of every product for each catgegory
    link_of_product(deal_pages(start(url_home_page)))
    # We try our scrqping code on a subset of urls that are the following
    urls = ["https://www.zabilo.com/en/top-loading-washing-machines/2629-constructa-top-loading-washer-6kg-1000rpm-cwt10r16il.html","https://www.zabilo.com/en/front-loading-washing-machines/2423-washing-machine-haier-hw80-1203-8kg-1200-rpm.html","https://www.zabilo.com/en/top-air-conditionner/3196-electra-air-conditioner-125-hp-12300-btu-platinum-140.html","https://www.zabilo.com/en/tvs-55/2308-fujicom-smart-tv-55-inches-4k-ultra-hd-fj554k.html"]
    # We scrape all informations for each Product
    get_product_characteristics(urls)
    # We create our dataframe for those urls
    df = get_data_frame(get_product_characteristics(urls))
    print(df)



if __name__ == '__main__':
    main()




