from setuptools import setup
setup(
     name='Helloworld',
     version='1.0',
     py_modules=['hello'],
     install_requires=['Click','requests','bs4',pandas,],
     scripts=[webscraping_project_part2.py,],
     entry_points="""[console_scripts] 
		     hello=hello:cli""",)
